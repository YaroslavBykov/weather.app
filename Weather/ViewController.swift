//
//  ViewController.swift
//  Weather
//
//  Created by Yaroslav Bykov on 20.10.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Charts

class ViewController: UIViewController {
    
    var tempJsonList: [StructModel] = []
    var tempCharts: [String]! = []
    var tempValuesCharts: [Double]! = []
    let defaults = UserDefaults.standard

    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var collectionView: UICollectionView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        barChart.chartDescription?.text = ""
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func loadData() {
        let url = Global.url + Global.appId
        Alamofire.request(url, method: .get).validate().responseJSON {
            response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                for i in 0..<json["cnt"].intValue {
                    if json["list"][i]["dt_txt"].stringValue.hasSuffix("12:00:00") {
                        let tempModel = StructModel(dateString: json["list"][i]["dt_txt"].stringValue,
                                                temperature: json["list"][i]["main"]["temp"].stringValue,
                                                pressure: json["list"][i]["main"]["pressure"].stringValue,
                                                humidity: json["list"][i]["main"]["humidity"].stringValue,
                                                mainInfo: json["list"][i]["weather"][0]["main"].stringValue,
                                                description: json["list"][i]["weather"][0]["description"].stringValue,
                                                iconId: json["list"][i]["weather"][0]["icon"].stringValue,
                                                windSpeed: json["list"][i]["wind"]["speed"].stringValue,
                                                windDegree: json["list"][i]["wind"]["deg"].stringValue)
                        
                        self.tempJsonList.append(tempModel)
                        self.tempValuesCharts.append(Double(tempModel.temperature)!)
                        self.tempCharts.append(tempModel.dateString)
                    }
                }
                if self.tempJsonList.count < Global.Period.first.rawValue {
                    self.tempJsonList.insert(StructModel(dateString: json["list"][0]["dt_txt"].stringValue,
                                                         temperature: json["list"][0]["main"]["temp"].stringValue,
                                                         pressure: json["list"][0]["main"]["pressure"].stringValue,
                                                         humidity: json["list"][0]["main"]["humidity"].stringValue,
                                                         mainInfo: json["list"][0]["weather"][0]["main"].stringValue,
                                                         description: json["list"][0]["weather"][0]["description"].stringValue,
                                                         iconId: json["list"][0]["weather"][0]["icon"].stringValue,
                                                         windSpeed: json["list"][0]["wind"]["speed"].stringValue,
                                                         windDegree: json["list"][0]["wind"]["deg"].stringValue), at: 0)
                }

                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.setChart(dataPoints: self.tempCharts, values: self.tempValuesCharts)
                    UserCache.save(self.tempJsonList)
                }
            case .failure:
                self.tempJsonList = UserCache.get()
                print("Data used from Cache")
            }
        }
    }
    
    func setChart(dataPoints: [String]!, values: [Double]) {
        barChart.delegate = self
        var dataEntries: [BarChartDataEntry] = []

        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }

        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Temperatures")
        let chartData = BarChartData(dataSet: chartDataSet)
        self.barChart.data = chartData
    }
}



        // MARK: Extensions

extension ViewController: ChartViewDelegate {
    public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        barChart = chartView as! BarChartView
        let indexScroll = Int(entry.x)
        collectionView.scrollToItem(at: IndexPath(row: indexScroll, section: 0), at: .right, animated: true)
    }
}


extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempJsonList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "CollectionCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CollectionViewCell
    
            let fullIconUrl = URL(string: Global.urlIcon + self.tempJsonList[indexPath.row].iconId + ".png")
        if let data = try? Data(contentsOf: fullIconUrl!) {
            cell.icon.image = UIImage(data: data)
        } else {
            cell.icon.image = UIImage(named: "Ooops")
        }

        cell.date.text = String(self.tempJsonList[indexPath.row].dateString.prefix(10))
        cell.info.text = self.tempJsonList[indexPath.row].mainInfo + ", " + self.tempJsonList[indexPath.row].description
        cell.temp.text = "Temp: " + self.tempJsonList[indexPath.row].temperature + "K"
        cell.pressure.text = "Pres: " + self.tempJsonList[indexPath.row].pressure + "gPa"
        cell.humidity.text = "Hum: " + self.tempJsonList[indexPath.row].humidity + "%"
        cell.windSpeed.text = "W. sp: " + self.tempJsonList[indexPath.row].windSpeed + "m/s"
        cell.windIcon.image = UIImage(named: "IconWind")
        cell.windIcon.transform = CGAffineTransform(rotationAngle: CGFloat(Float(self.tempJsonList[indexPath.row].windDegree)!))
    
        return cell
    }
    

}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("hi")
    }
}
