//
//  DataLoader.swift
//  Weather
//
//  Created by Yaroslav Bykov on 29.10.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

//import Foundation
//import Alamofire
//import SwiftyJSON
//
//class DataLoader {
//    var tempJsonList: [StructModel] = []
//        let url = Global.url + Global.appId
//        Alamofire.request(url, method: .get).validate().responseJSON {
//            response in
//            switch response.result {
//            case .success(let value):
//                let json = JSON(value)
//                for i in 0..<json["cnt"].intValue {
//                    if json["list"][i]["dt_txt"].stringValue.hasSuffix("12:00:00") {
//                        let tempModel = StructModel(dateString: json["list"][i]["dt_txt"].stringValue,
//                                                    temperature: json["list"][i]["main"]["temp"].stringValue,
//                                                    pressure: json["list"][i]["main"]["pressure"].stringValue,
//                                                    humidity: json["list"][i]["main"]["humidity"].stringValue,
//                                                    mainInfo: json["list"][i]["weather"][0]["main"].stringValue,
//                                                    description: json["list"][i]["weather"][0]["description"].stringValue,
//                                                    iconId: json["list"][i]["weather"][0]["icon"].stringValue,
//                                                    windSpeed: json["list"][i]["wind"]["speed"].stringValue,
//                                                    windDegree: json["list"][i]["wind"]["deg"].stringValue)
//
//                        self.tempJsonList.append(tempModel)
//                    }
//                }
//                if self.tempJsonList.count < Global.Period.first.rawValue {
//                    self.tempJsonList.insert(StructModel(dateString: json["list"][0]["dt_txt"].stringValue,
//                                                         temperature: json["list"][0]["main"]["temp"].stringValue,
//                                                         pressure: json["list"][0]["main"]["pressure"].stringValue,
//                                                         humidity: json["list"][0]["main"]["humidity"].stringValue,
//                                                         mainInfo: json["list"][0]["weather"][0]["main"].stringValue,
//                                                         description: json["list"][0]["weather"][0]["description"].stringValue,
//                                                         iconId: json["list"][0]["weather"][0]["icon"].stringValue,
//                                                         windSpeed: json["list"][0]["wind"]["speed"].stringValue,
//                                                         windDegree: json["list"][0]["wind"]["deg"].stringValue), at: 0)
//                }
//    
//                DispatchQueue.main.async {
//                    self.collectionView.reloadData()
//                    self.setChart(dataPoints: self.tempCharts, values: self.tempValuesCharts)
//                }
//            case .failure(let error):
//                print(error)
//            }
//        }
//}

    

