//
//  UserCache.swift
//  Weather
//
//  Created by Yaroslav Bykov on 29.10.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import Foundation

struct UserCache {
    static let key = "Cache"
    
    static func save(_ value: [StructModel]!) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
        print("Cache saved")
    }
    
    static func get() -> [StructModel]! {
        var weatherData: [StructModel]!
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            weatherData = try? PropertyListDecoder().decode([StructModel].self, from: data)
            return weatherData!
        } else {
            return weatherData
        }
    }
}
