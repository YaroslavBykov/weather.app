//
//  CollectionViewCell.swift
//  Weather
//
//  Created by Yaroslav Bykov on 24.10.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var info: UILabel!
    
    @IBOutlet weak var temp: UILabel!
    
    @IBOutlet weak var pressure: UILabel!
    
    @IBOutlet weak var humidity: UILabel!
    
    @IBOutlet weak var windSpeed: UILabel!
    
    @IBOutlet weak var windIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
