//
//  StructModel.swift
//  Weather
//
//  Created by Yaroslav Bykov on 20.10.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import UIKit

struct StructModel: Codable {
    var dateString: String
    let temperature: String
    let pressure: String
    let humidity: String
    let mainInfo: String
    let description: String
    let iconId: String
    let windSpeed: String
    let windDegree: String
}
