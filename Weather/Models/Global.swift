//
//  Global.swift
//  Weather
//
//  Created by Yaroslav Bykov on 22.10.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import Foundation

struct Global {
    static let url = "https://api.openweathermap.org/data/2.5/forecast?id=524901&appid="
    static let appId = "8b3ca614026eaa6d0f65a7264430b25a"
    static let urlIcon = "https://openweathermap.org/img/w/"
    enum Period: Int {
        case first = 5
        case second = 10
    }
}
